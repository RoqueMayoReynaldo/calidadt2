﻿using CalidadT2.Authentication;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class LibroControllerTest
    {
        [Test]
        public void DetailsCaso01()
        {
            var mockLibroRepo = new Mock<ILibroRepository>();

            LibroController lc = new LibroController(mockLibroRepo.Object, null, null,null);

            Assert.IsInstanceOf<ViewResult>(lc.Details(1));

        }
        [Test]
        public void DetailsCaso02()
        {
            var mockLibroRepo = new Mock<ILibroRepository>();
            mockLibroRepo.Setup(o => o.GetBookWithAuthorComents(It.IsAny<int>())).Returns(new Libro());


            LibroController lc = new LibroController(mockLibroRepo.Object, null, null,null);

            var resultado = lc.Details(1) as ViewResult;

            Assert.IsNotNull(resultado.Model);

        }

        [Test]
        public void AddComentarioCaso01()
        {

          

            var mockLibroRepo = new Mock<ILibroRepository>();
            mockLibroRepo.Setup(o=>o.GetBook(It.IsAny<Comentario>())).Returns(new Libro());

            var mockComentRepo = new Mock<IComentarioRepository>();

            mockComentRepo.Setup(o=>o.AddComentary(It.IsAny<Comentario>()));

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockAuth = new Mock<IAutenticacion>();
                 
          
            LibroController lc = new LibroController(mockLibroRepo.Object, mockComentRepo.Object, mockUsuarioRepo.Object,mockAuth.Object);


            Assert.IsInstanceOf<RedirectToActionResult>(lc.AddComentario(new Comentario()));

        }

     

    }
}
