﻿using CalidadT2.Authentication;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class AuthControllerTest
    {

        [Test]
        public void LoginCaso01Correcto()
        {
            var mockRepo = new Mock<IUsuarioRepository>();

      
            mockRepo.Setup(o => o.FindByCredentials("Juan", "1234")).Returns(new Usuario());

            var mockAuth = new Mock<IAutenticacion>();

            AuthController ac = new AuthController( mockAuth.Object, mockRepo.Object);

            var resultado = ac.Login("Juan", "1234");

            Assert.IsInstanceOf<RedirectToActionResult>(resultado);

        }


        [Test]
        public void LoginCaso02Correcto()
        {
            var mockRepo = new Mock<IUsuarioRepository>();


            mockRepo.Setup(o => o.FindByCredentials("Juan", "1234")).Returns(new Usuario());

            var mockAuth = new Mock<IAutenticacion>();

            AuthController ac = new AuthController(mockAuth.Object, mockRepo.Object);

            var resultado = ac.Login("Juan", "1234");

            Assert.IsNotNull(resultado);

        }
        [Test]
        public void LoginCaso01Fallo()
        {
            var mockRepo = new Mock<IUsuarioRepository>();

            mockRepo.Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>())).Returns((Usuario)null);

            var mockAuth = new Mock<IAutenticacion>();
            AuthController ac = new AuthController(mockAuth.Object,mockRepo.Object);

            var resultado = ac.Login("AlexRM", "1234");

            Assert.IsInstanceOf<ViewResult>(resultado);
    

        }

        [Test]
        public void LoginCaso02Fallo()
        {
            var mockRepo = new Mock<IUsuarioRepository>();

            mockRepo.Setup(o => o.FindByCredentials(It.IsAny<String>(), It.IsAny<String>())).Returns((Usuario)null);

            var mockAuth = new Mock<IAutenticacion>();
            AuthController ac = new AuthController(mockAuth.Object, mockRepo.Object);

            var resultado = ac.Login("AlexRM", "1234") as ViewResult;

            Assert.IsNotNull(ac.ViewBag);


        }
    }
}
