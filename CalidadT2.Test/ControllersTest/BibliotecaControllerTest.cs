﻿using CalidadT2.Authentication;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class BibliotecaControllerTest
    {

        [Test]
        public void IndexCaso01()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();          

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o=>o.GetBibliotecas(It.IsAny<int>())).Returns(new List<Biblioteca>());
           

            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object,mockBiblioRepo.Object);

            Assert.IsInstanceOf<ViewResult>(bc.Index());

        }

        [Test]
        public void IndexCaso02()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.GetBibliotecas(It.IsAny<int>())).Returns(new List<Biblioteca>());


            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var resultado = bc.Index() as ViewResult;

            Assert.NotNull(resultado.Model);

        }



        [Test]
        public void AddCaso01()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.AddBiblioteca(It.IsAny<Biblioteca>()));
      
            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();
           

            bc.TempData = mockTempData.Object;

            Assert.IsInstanceOf<RedirectToActionResult>(bc.Add(1));

        }

        [Test]
        public void AddCaso02()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.AddBiblioteca(It.IsAny<Biblioteca>()));

            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();


            bc.TempData = mockTempData.Object;

            Assert.IsNotNull(bc.TempData);

        }


        [Test]
        public void MarcarComoLeyendoCaso01()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.GetBookBiblio(It.IsAny<int>(), It.IsAny<int>())).Returns(new Biblioteca() );



            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();


            bc.TempData = mockTempData.Object;

            Assert.IsInstanceOf<RedirectToActionResult>(bc.MarcarComoLeyendo(2));

        }

        [Test]
        public void MarcarComoLeyendoCaso02()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.GetBookBiblio(It.IsAny<int>(), It.IsAny<int>())).Returns(new Biblioteca());



            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();


            bc.TempData = mockTempData.Object;
            Assert.IsInstanceOf<RedirectToActionResult>(bc.MarcarComoLeyendo(10));
            Assert.IsNotNull(bc.TempData);

        }


        [Test]
        public void MarcarComoTerminadoCaso01()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.GetBookBiblio(It.IsAny<int>(), It.IsAny<int>())).Returns(new Biblioteca());



            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();


            bc.TempData = mockTempData.Object;

            Assert.IsInstanceOf<RedirectToActionResult>(bc.MarcarComoTerminado(2));
      

        }
        [Test]
        public void MarcarComoTerminadoCaso02()
        {
            var mockAuth = new Mock<IAutenticacion>();

            var mockUsuarioRepo = new Mock<IUsuarioRepository>();

            mockUsuarioRepo.Setup(o => o.GetUsuario(It.IsAny<String>())).Returns(new Usuario());

            var mockBiblioRepo = new Mock<IBibliotecaRepository>();

            mockBiblioRepo.Setup(o => o.GetBookBiblio(It.IsAny<int>(), It.IsAny<int>())).Returns(new Biblioteca());



            BibliotecaController bc = new BibliotecaController(mockAuth.Object, mockUsuarioRepo.Object, mockBiblioRepo.Object);

            var mockTempData = new Mock<ITempDataDictionary>();


            bc.TempData = mockTempData.Object;

            Assert.IsInstanceOf<RedirectToActionResult>(bc.MarcarComoTerminado(2));
            Assert.IsNotNull(bc.TempData);

        }
    }
}
