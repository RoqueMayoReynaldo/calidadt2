﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.ControllersTest
{
    public class HomeControllerTest
    {

        [Test]
        public void IndexCaso01()
        {
            var mockLibroRep = new Mock<ILibroRepository>();

            mockLibroRep.Setup(o => o.GetBooskWithAuthor()).Returns(new List<Libro>());


            HomeController hc = new HomeController(mockLibroRep.Object);

            Assert.IsInstanceOf<ViewResult>(hc.Index());

        }

    }
}
