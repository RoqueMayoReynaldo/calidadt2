﻿using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Test.RepositoryTest.Mock;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.RepositoryTest
{
    public class ComentarioRepositoryTest
    {

        private Mock<AppBibliotecaContext> mockContext;

        [SetUp]

        public void SetUp()
        {

            mockContext = AplicationContextMock.GetContextMock();
        }


        [Test]

        public void AddComentaryTestCaso01()
        {
            Comentario c = new Comentario { Id = 1, LibroId = 1, UsuarioId = 1, Texto = "abcd", Fecha = new DateTime(2021, 11, 10), Puntaje = 4 };
            ComentarioRepository cr = new ComentarioRepository(mockContext.Object);
            cr.AddComentary(c);

        }

       
    }
}
