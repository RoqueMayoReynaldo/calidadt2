﻿using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Test.RepositoryTest.Mock;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.RepositoryTest
{
    public class LibroRepositoryTest
    {

        private Mock<AppBibliotecaContext> mockContext;

        [SetUp]

        public void SetUp()
        {

            mockContext = AplicationContextMock.GetContextMock();
        }


        [Test]
        public void GetBooskWithAuthorCaso01()
        {
            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.IsNotNull(lr.GetBooskWithAuthor());


        }

        [Test]
        public void GetBooskWithAuthorCaso02()
        {
            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.AreEqual(6,lr.GetBooskWithAuthor().Count);

          
        }

        [Test]
        public void GetBookWithAuthorComentsCaso01()
        {
            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.AreEqual(1, lr.GetBookWithAuthorComents(1).Id);


        }
        [Test]
        public void GetBookWithAuthorComentsCaso02()
        {
            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.IsNotNull(lr.GetBookWithAuthorComents(1));



        }

        [Test]
        public void GetBookCaso01()
        {
            Comentario c = new Comentario { Id = 2, LibroId = 2, UsuarioId = 2, Texto = "abcd", Fecha = new DateTime(2020, 11, 11), Puntaje = 4 };

            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.AreEqual(2, lr.GetBook(c).Id);


        }
        [Test]
        public void GetBookCaso02()
        {
            Comentario c = new Comentario { Id = 2, LibroId = 2, UsuarioId = 2, Texto = "abcd", Fecha = new DateTime(2020, 11, 11), Puntaje = 4 };

            LibroRepository lr = new LibroRepository(mockContext.Object);

            Assert.IsNotNull(lr.GetBook(c));

        }
    }
}
