﻿using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Test.RepositoryTest.Mock;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.RepositoryTest
{
    public class BibliotecaRepositoryTest
    {


        private Mock<AppBibliotecaContext> mockContext;

        [SetUp]

        public void SetUp()
        {

            mockContext = AplicationContextMock.GetContextMock();
        }


        [Test]
        public void GetBibliotecasCaso01()
        {

            BibliotecaRepository br = new BibliotecaRepository(mockContext.Object);

            Assert.AreEqual(1, br.GetBibliotecas(1).Count);

        }

        [Test]
        public void GetBibliotecasCaso02()
        {

            BibliotecaRepository br = new BibliotecaRepository(mockContext.Object);

            Assert.IsNotNull(br.GetBibliotecas(2));

        }

        [Test]
        public void GetBookBiblioCaso01()
        {
            BibliotecaRepository br = new BibliotecaRepository(mockContext.Object);

            Assert.AreEqual(2,br.GetBookBiblio(2,2).Id);

        }

        [Test]
        public void GetBookBiblioCaso02()
        {
            BibliotecaRepository br = new BibliotecaRepository(mockContext.Object);

            Assert.IsNull(br.GetBookBiblio(4,1));

        }
    }
}
