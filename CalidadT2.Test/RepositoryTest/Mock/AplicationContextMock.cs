﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalidadT2.Test.RepositoryTest.Mock
{
    public static class AplicationContextMock
    {
        public static Mock<AppBibliotecaContext> GetContextMock()
        {
            IQueryable<Usuario> userData = GetUserData();

            var mockSetUsuario = new Mock<DbSet<Usuario>>();
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.Provider).Returns(userData.Provider);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.Expression).Returns(userData.Expression);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.ElementType).Returns(userData.ElementType);
            mockSetUsuario.As<IQueryable<Usuario>>().Setup(m => m.GetEnumerator()).Returns(userData.GetEnumerator());


            mockSetUsuario.Setup(m => m.AsQueryable()).Returns(userData);

            ///////////////

            IQueryable<Libro> libroData = GetLibroData();

            var mockSetLibro = new Mock<DbSet<Libro>>();
            mockSetLibro.As<IQueryable<Libro>>().Setup(m => m.Provider).Returns(libroData.Provider);
            mockSetLibro.As<IQueryable<Libro>>().Setup(m => m.Expression).Returns(libroData.Expression);
            mockSetLibro.As<IQueryable<Libro>>().Setup(m => m.ElementType).Returns(libroData.ElementType);
            mockSetLibro.As<IQueryable<Libro>>().Setup(m => m.GetEnumerator()).Returns(libroData.GetEnumerator());

         
            mockSetLibro.Setup(m => m.AsQueryable()).Returns(libroData);




            IQueryable<Comentario> comentarioData = GetComentarioData();

            var mockSetComentario = new Mock<DbSet<Comentario>>();
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.Provider).Returns(comentarioData.Provider);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.Expression).Returns(comentarioData.Expression);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.ElementType).Returns(comentarioData.ElementType);
            mockSetComentario.As<IQueryable<Comentario>>().Setup(m => m.GetEnumerator()).Returns(comentarioData.GetEnumerator());

            //añadimos esto para los querys
            mockSetComentario.Setup(m => m.AsQueryable()).Returns(comentarioData);





            IQueryable<Biblioteca> bibliotecaData = GetBibliotecaData();

            var mockSetBiblioteca = new Mock<DbSet<Biblioteca>>();
            mockSetBiblioteca.As<IQueryable<Biblioteca>>().Setup(m => m.Provider).Returns(bibliotecaData.Provider);
            mockSetBiblioteca.As<IQueryable<Biblioteca>>().Setup(m => m.Expression).Returns(bibliotecaData.Expression);
            mockSetBiblioteca.As<IQueryable<Biblioteca>>().Setup(m => m.ElementType).Returns(bibliotecaData.ElementType);
            mockSetBiblioteca.As<IQueryable<Biblioteca>>().Setup(m => m.GetEnumerator()).Returns(bibliotecaData.GetEnumerator());

            //añadimos esto para los querys
            mockSetBiblioteca.Setup(m => m.AsQueryable()).Returns(bibliotecaData);




     








            //creamos el mock del context , enviando su parametro necesario
            var mockContext = new Mock<AppBibliotecaContext>(new DbContextOptions<AppBibliotecaContext>());

            mockContext.Setup(c => c.Usuarios).Returns(mockSetUsuario.Object);

            mockContext.Setup(c => c.Libros).Returns(mockSetLibro.Object);

            mockContext.Setup(c => c.Comentarios).Returns(mockSetComentario.Object);

            mockContext.Setup(c => c.Bibliotecas).Returns(mockSetBiblioteca.Object);

    

            return mockContext;

        }



        private static IQueryable<Biblioteca> GetBibliotecaData()
        {

            return new List<Biblioteca>
            {

               new Biblioteca{ Id=1,UsuarioId=1,LibroId=1,Estado=1,Usuario=GetUserData().FirstOrDefault(o=>o.Id==1),Libro=GetLibroData().FirstOrDefault(o=>o.Id==1)},
               new Biblioteca{ Id=2,UsuarioId=2,LibroId=2,Estado=0,Usuario=GetUserData().FirstOrDefault(o=>o.Id==2),Libro=GetLibroData().FirstOrDefault(o=>o.Id==2)},
               new Biblioteca{ Id=3,UsuarioId=3,LibroId=3,Estado=1,Usuario=GetUserData().FirstOrDefault(o=>o.Id==3),Libro=GetLibroData().FirstOrDefault(o=>o.Id==3)},
               new Biblioteca{ Id=4,UsuarioId=4,LibroId=4,Estado=1,Usuario=GetUserData().FirstOrDefault(o=>o.Id==4),Libro=GetLibroData().FirstOrDefault(o=>o.Id==4)},
               new Biblioteca{ Id=5,UsuarioId=5,LibroId=5,Estado=1,Usuario=GetUserData().FirstOrDefault(o=>o.Id==5),Libro=GetLibroData().FirstOrDefault(o=>o.Id==5)}


            }.AsQueryable();

        }


        

        private static IQueryable<Comentario> GetComentarioData()
        {
            return new List<Comentario>
            {

                new Comentario{ Id=1,LibroId=1,UsuarioId=1,Texto="abcd",Fecha=new DateTime(2021,11,10),Puntaje=4,Usuario=GetUserData().FirstOrDefault(o=>o.Id==1)},
                new Comentario{ Id=2,LibroId=2,UsuarioId=2,Texto="abcd",Fecha=new DateTime(2020,11,11),Puntaje=4,Usuario=GetUserData().FirstOrDefault(o=>o.Id==2)},
                new Comentario{ Id=3,LibroId=3,UsuarioId=3,Texto="abcd",Fecha=new DateTime(2019,11,12),Puntaje=4,Usuario=GetUserData().FirstOrDefault(o=>o.Id==3)},
                new Comentario{ Id=4,LibroId=4,UsuarioId=4,Texto="abcd",Fecha=new DateTime(2018,11,13),Puntaje=4,Usuario=GetUserData().FirstOrDefault(o=>o.Id==4)},
                new Comentario{ Id=5,LibroId=5,UsuarioId=5,Texto="abcd",Fecha=new DateTime(2017,11,14),Puntaje=4,Usuario=GetUserData().FirstOrDefault(o=>o.Id==5)},


            }.AsQueryable();
        }


      

        private static IQueryable<Libro> GetLibroData()
        {
            return new List<Libro>
            {

                new Libro{ Id=1,Nombre="Libro1",Imagen="123",AutorId=1,Puntaje=13,Autor=new Autor{Id=1,Nombres="Autor1" },Comentarios=GetComentarioData().Where(o=>o.LibroId==1).ToList() },
                new Libro{ Id=2,Nombre="Libro2",Imagen="123",AutorId=2,Puntaje=13,Autor=new Autor{Id=2,Nombres="Autor2" },Comentarios=GetComentarioData().Where(o=>o.LibroId==2).ToList() },
                new Libro{ Id=3,Nombre="Libro3",Imagen="123",AutorId=3,Puntaje=13,Autor=new Autor{Id=3,Nombres="Autor3" },Comentarios=GetComentarioData().Where(o=>o.LibroId==3).ToList() },
                new Libro{ Id=4,Nombre="Libro4",Imagen="123",AutorId=4,Puntaje=13,Autor=new Autor{Id=4,Nombres="Autor4" },Comentarios=GetComentarioData().Where(o=>o.LibroId==4).ToList() },
                new Libro{ Id=5,Nombre="Libro5",Imagen="123",AutorId=4,Puntaje=13,Autor=new Autor{Id=5,Nombres="Autor5" },Comentarios=GetComentarioData().Where(o=>o.LibroId==5).ToList() },
                new Libro{ Id=6,Nombre="Libro6",Imagen="123",AutorId=3,Puntaje=13,Autor=new Autor{Id=6,Nombres="Autor6" },Comentarios=GetComentarioData().Where(o=>o.LibroId==6).ToList() }



            }.AsQueryable();


        }

        private static IQueryable<Usuario> GetUserData()
        {
            return new List<Usuario>
            {
                new Usuario { Id=1,Nombres="Raul",Password="123456",Username="Raul123" },
                new Usuario { Id=2,Nombres="Juan",Password="123456",Username="Juan123" },
                new Usuario { Id=3,Nombres="Rosa",Password="123456",Username="Rosa123" },
                new Usuario { Id=4,Nombres="Paul",Password="123456",Username="Paul123" },
                new Usuario { Id=5,Nombres="Ines",Password="123456",Username="Ines123" }



            }.AsQueryable();
        }
    }
}
