﻿using CalidadT2.Models;
using CalidadT2.Repository;
using CalidadT2.Test.RepositoryTest.Mock;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Test.RepositoryTest
{
    public class UsuarioRepositoryTest
    {

        private Mock<AppBibliotecaContext> mockContext;

        [SetUp]

        public void SetUp()
        {

            mockContext = AplicationContextMock.GetContextMock();
        }



        [Test]
        public void FindByCredentialsCaso01()
        {

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var resultado = ur.FindByCredentials("Paul123", "123456");

            Assert.AreEqual(4, resultado.Id);

        }

        [Test]
        public void FindByCredentialsCaso02()
        {

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var resultado = ur.FindByCredentials("11111", "123");

            Assert.IsNull(resultado);

        }

        [Test]
        public void GetUsuarioCaso01()
        {

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var resultado = ur.GetUsuario("Paul123");
            Assert.AreEqual(4, resultado.Id);

        }

        [Test]
        public void GetUsuarioCaso02()
        {

            UsuarioRepository ur = new UsuarioRepository(mockContext.Object);

            var resultado = ur.GetUsuario("14414");
            Assert.IsNull(resultado);

        }
    }
}
