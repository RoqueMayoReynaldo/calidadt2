﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Repository;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILibroRepository libroRepo;

        public HomeController(ILibroRepository libroRepo)
        {
            this.libroRepo = libroRepo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = libroRepo.GetBooskWithAuthor();
            return View(model);
        }
    }
}
