﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Authentication;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly IAutenticacion autenticacion;
        private readonly IUsuarioRepository usuarioRepo;
        private readonly IBibliotecaRepository bibliotecaRepo;

        public BibliotecaController(IAutenticacion autenticacion,IUsuarioRepository usuarioRepo,IBibliotecaRepository bibliotecaRepo)
        {
            this.autenticacion = autenticacion;
            this.usuarioRepo = usuarioRepo;
            this.bibliotecaRepo = bibliotecaRepo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Usuario user = LoggedUser();

            var model = bibliotecaRepo.GetBibliotecas(user.Id);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();

            var biblioteca = new Biblioteca
            {
                LibroId = libro,
                UsuarioId = user.Id,
                Estado = ESTADO.POR_LEER
            };

           

            bibliotecaRepo.AddBiblioteca(biblioteca);

            TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser();

            
            var libro = bibliotecaRepo.GetBookBiblio(libroId,user.Id);

            libro.Estado = ESTADO.LEYENDO;

            bibliotecaRepo.GuardarCambios();

            TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser();


            var libro = bibliotecaRepo.GetBookBiblio(libroId,user.Id);

            libro.Estado = ESTADO.TERMINADO;
            
            bibliotecaRepo.GuardarCambios();

            TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {

            var user = usuarioRepo.GetUsuario(autenticacion.GetClaimUser());
            return user;
        }
    }
}
