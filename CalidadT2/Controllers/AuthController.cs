﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CalidadT2.Authentication;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CalidadT2.Controllers
{


    public class AuthController : Controller
    {
        private readonly IAutenticacion autenticacion;
        private readonly IUsuarioRepository usuarioRepo;

        public AuthController(IAutenticacion autenticacion,IUsuarioRepository usuarioRepo)
        {
            this.autenticacion = autenticacion;
            this.usuarioRepo = usuarioRepo;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var usuario = usuarioRepo.FindByCredentials(username,password);
            if (usuario != null)
            {



                autenticacion.SigIn(username);
                return RedirectToAction("Index", "Home");
            }
            
            ViewBag.Validation = "Usuario y/o contraseña incorrecta";
            return View();
        }


        public ActionResult Logout()
        {

            autenticacion.CloseSession();        
            return RedirectToAction("Login");
        }
    }
}
