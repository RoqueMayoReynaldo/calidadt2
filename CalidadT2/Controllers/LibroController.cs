﻿using System;
using System.Linq;
using System.Security.Claims;
using CalidadT2.Authentication;
using CalidadT2.Models;
using CalidadT2.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroRepository libroRepo;
        private readonly IComentarioRepository comentarioRepo;
        private readonly IUsuarioRepository usuarioRepo;
        private readonly IAutenticacion autenticacion;

        public LibroController(ILibroRepository libroRepo,IComentarioRepository comentarioRepo,IUsuarioRepository usuarioRepo, IAutenticacion autenticacion)
        {
            this.libroRepo = libroRepo;
            this.comentarioRepo = comentarioRepo;
            this.usuarioRepo = usuarioRepo;
            this.autenticacion = autenticacion;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = libroRepo.GetBookWithAuthorComents(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;


            comentarioRepo.AddComentary(comentario);

           

            var libro = libroRepo.GetBook(comentario);

            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            libroRepo.GuardarCambios();
            comentarioRepo.GuardarCambios();

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
                   
            var user = usuarioRepo.GetUsuario(autenticacion.GetClaimUser());
            return user;
        }
    }
}
