﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repository
{
    public interface IComentarioRepository
    {

        void AddComentary(Comentario comentario);
        void GuardarCambios();

    }
    public class ComentarioRepository : IComentarioRepository
    {
        private readonly AppBibliotecaContext context;

        public ComentarioRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }

        public void AddComentary(Comentario comentario)
        {
            context.Comentarios.Add(comentario);
        }

        public void GuardarCambios()
        {


            context.SaveChanges();
        }

    }
}
