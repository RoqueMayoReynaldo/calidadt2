﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repository
{
    public interface ILibroRepository {

        List<Libro> GetBooskWithAuthor();
        Libro GetBookWithAuthorComents(int id);
        Libro GetBook(Comentario comentario);

        void GuardarCambios();

    }
    public class LibroRepository : ILibroRepository
    {
        private readonly AppBibliotecaContext context;

        public LibroRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }


        public List<Libro> GetBooskWithAuthor()
        {
            return context.Libros.Include(o => o.Autor).ToList();
        }

        public Libro GetBookWithAuthorComents(int id)
        {
            return context.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == id)
                .FirstOrDefault();
        }

        public Libro GetBook(Comentario comentario)
        {
            return context.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
        }

        public void GuardarCambios()
        {


            context.SaveChanges();
        }
    }
}
