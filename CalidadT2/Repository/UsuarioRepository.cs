﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repository
{

    public interface IUsuarioRepository
    {

        Usuario FindByCredentials(String userName,String pass);
        Usuario GetUsuario(String username);

    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppBibliotecaContext context;

        public UsuarioRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }

        public Usuario FindByCredentials(string userName, string pass)
        {
            return context.Usuarios.FirstOrDefault(o=>o.Username==userName && o.Password==pass);
        }

        public Usuario GetUsuario(String username)
        {

            return context.Usuarios.Where(o => o.Username == username).FirstOrDefault();

        }
            
    }
}
