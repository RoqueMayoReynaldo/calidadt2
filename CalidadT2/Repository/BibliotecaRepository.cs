﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repository

{ 
    public interface IBibliotecaRepository
    {
        List<Biblioteca> GetBibliotecas(int id);
        void AddBiblioteca(Biblioteca biblioteca);
        Biblioteca GetBookBiblio(int libroId, int UserId);
        void GuardarCambios();
    }

    public class BibliotecaRepository : IBibliotecaRepository
    {
        private readonly AppBibliotecaContext context;

        public BibliotecaRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }

        public List<Biblioteca> GetBibliotecas(int id)
        {

            return context.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == id)
                .ToList();
        }
        public void AddBiblioteca(Biblioteca biblioteca)
        {
            context.Bibliotecas.Add(biblioteca);
            context.SaveChanges();

        }

        public Biblioteca GetBookBiblio(int libroId, int UserId)
        {

            return context.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == UserId)
                .FirstOrDefault();
        }

        public void GuardarCambios()
        {


            context.SaveChanges();
        }
    }
}
