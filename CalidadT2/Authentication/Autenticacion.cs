﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Authentication
{
    public interface IAutenticacion
    {

        void SigIn(String usName);
        void CloseSession();

        String GetClaimUser();


    }

    public class Autenticacion : IAutenticacion
    {

        private readonly IHttpContextAccessor _httpContextAccessor;

        public Autenticacion(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
 

        public void CloseSession()
        {
            _httpContextAccessor.HttpContext.SignOutAsync();
           
        }

        public String GetClaimUser()
        {


            return _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault().Value;
        }

        public void SigIn(string usName)
        {
            var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, usName)
                };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);


            _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal);
        }
    }
}
